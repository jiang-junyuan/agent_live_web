import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'


import permission from './permission'
import module from './module'
import login from './login'
import live from './live'
import host from './host'
import video from './video'
import goods from './goods'
import order from './order'
import user from './user'

Vue.use(Router)
Vue.use(ElementUI)

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

let routeList = [
  module.routes,
  permission.routes,
  login.routes,
  live.routes,
  host.routes,
  video.routes,
  goods.routes,
  order.routes,
  user.routes,
]

let routes = []

//拼接路由
routeList.map(s => {
  routes = routes.concat(s)
})

const router = new Router({
  routes: routes
})

// router.beforeEach((to, from, next) => {

  /**
   * @author: chenwei
   * @Date: 2020-08-31 17:15:11
   * @description: "未登录拦截"
   */
  // let token = localStorage.getItem('agent_live_web_token')
  // if (token) {
  //   next()
  // } else {
  //   if (to.path === '/login' || to.path === '/') {
  //     next()
  //   }else if(to.path === '/login/agreement'){
  //     next()
  //   }else {
  //     next({ path: '/login' })
  //   }
  // }
// })


router.afterEach(route => {
  //将滚动条恢复到最顶端
  window.scrollTo(0, 0)
})

export default router
