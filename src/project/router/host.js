const host = {
  routes: [
    {
      path: '/host/list',
      name: 'hostList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '主播列表',
            path: '/host/list'
          }
        ]
      },
      component: () => import('@/project/views/host/list.vue')
    }
  ]
}
export default host;
