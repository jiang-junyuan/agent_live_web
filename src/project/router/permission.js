const permission = {
  routes: [
    {
      path: '/permission/list',
      name: 'permissionList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: true,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '功能模块',
            path: '/permission/list'
          }
        ]
      },
      component: () => import('@/project/views/permission/list')
    }
  ]
}
export default permission
