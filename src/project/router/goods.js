const goods = {
  routes: [
    {
      path: '/goods/list',
      name: 'goodsList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '商品列表',
            path: '/goods/list'
          }
        ]
      },
      component: () => import('@/project/views/goods/list.vue')
    }
  ]
}
export default goods;
