const order = {
  routes: [
    {
      path: '/order/list',
      name: 'orderList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '订单列表',
            path: '/order/list'
          }
        ]
      },
      component: () => import('@/project/views/order/list.vue')
    },
    {
      path: '/order/list/show/:id',
      name: 'orderListShow',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '订单列表',
            path: '/order/list'
          },
          {
            name: '查看',
            path: '/order/list/show/:id'
          },
        ]
      },
      component: () => import('@/project/views/order/show.vue')
    },
  ]
}
export default order;
