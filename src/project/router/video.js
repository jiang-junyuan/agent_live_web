const video = {
  routes: [
    {
      path: '/video/draft',
      name: 'shortVideoDraft',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '草稿箱',
            path: '/video/draft'
          }
        ]
      },
      component: () => import('@/project/views/short-video/draft.vue')
    },
    {
      path: '/video/published',
      name: 'shortVideoPublished',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '已发布视频',
            path: '/video/published'
          }
        ]
      },
      component: () => import('@/project/views/short-video/published.vue')
    },
    {
      path: '/video/draft/show/:id',
      name: 'videoDraftShow',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '草稿箱',
            path: '/video/draft'
          },
          {
            name:'短视频详情',
            path: '/video/draft/show/:id'
          }
        ]
      },
      component: () => import('@/project/views/short-video/draftShow.vue')
    },
    {
      path: '/video/published/show/:id',
      name: 'videoPublishedShow',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '已发布视频',
            path: '/video/published'
          },
          {
            name:'短视频详情',
            path: '/video/published/show/:id'
          }
        ]
      },
      // component: () => import('@/project/views/short-video/publishedShow.vue')
      component: () => import('@/project/views/short-video/draftShow.vue')
    },
  ]
}
export default video;
