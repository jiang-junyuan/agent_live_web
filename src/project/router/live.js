const live = {
  routes: [
    {
      path: '/live/list',
      name: 'liveList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播间管理',
            path: '/live/list'
          }
        ]
      },
      component: () => import('@/project/views/live/list.vue')
    },
    {
      path: '/live/playback',
      name: 'livePlayback',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播回放管理',
            path: '/live/playback'
          },

        ]
      },
      component: () => import('@/project/views/live/playback.vue')
    },
    {
      path: '/live/list/console/:id',
      name: 'console',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播间管理',
            path: '/live/list'
          },
          {
            name: '直播间详情',
            path: '/live/list/console/:id'
          }
        ]
      },
      component: () => import('@/project/views/live/console.vue')
    },
    {
      path: '/live/playback/show/:id',
      name: 'playbackShow',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播回放管理',
            path: '/live/playback'
          },
          {
            name: '直播回放详情',
            path: '/live/playback/show/:id'
          }
        ]
      },
      component: () => import('@/project/views/live/show.vue')
    },
    {
      path: '/live/playback/show/dataStatistics/:id',
      name: 'showDataStatistics',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播回放管理',
            path: '/live/playback'
          },
          {
            name: '直播回放详情',
            path: '/live/playback/show/:id'
          },
          {
            name: '直播间数据统计',
            path: '/live/playback/show/dataStatistics/:id'
          }
        ]
      },
      component: () => import('@/project/views/live/playbackData.vue')
    },
    {
      path: '/live/playback/console/dataStatistics/:id',
      name: 'consoleDataStatistics',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '直播回放管理',
            path: '/live/playback'
          },
          {
            name: '直播间详情',
            path: '/live/list/console/:id'
          },
          {
            name: '直播间数据统计',
            path: '/live/playback/console/dataStatistics/:id'
          }
        ]
      },
      component: () => import('@/project/views/live/listData.vue')
    }
  ]
}
export default live;
