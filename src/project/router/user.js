const user = {
  routes: [
    {
      path: '/user/info',
      name: 'userInfo',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '用户中心',
            path: '/user/info'
          }
        ]
      },
      component: () => import('@/project/views/user/info.vue')
    },
    {
      path: '/user/logon',
      name: 'userLogon',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '登录日志',
            path: '/user/logon'
          }
        ]
      },
      component: () => import('@/project/views/user/logon.vue')
    },
    {
      path: '/user/operation',
      name: 'userOperation',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: false,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '操作日志',
            path: '/user/operation'
          }
        ]
      },
      component: () => import('@/project/views/user/operation.vue')
    }
  ]
}
export default user
