const module = {
  routes: [
    {
      path: '/module/list',
      name: 'moduleList',
      meta: {
        isShowHeader: true,
        isShowLeftSider: true,
        keepAlive: true,
        breadcrumb: [
          {
            name: '首页',
            path: '/index'
          },
          {
            name: '功能模块',
            path: '/module/list'
          }
        ]
      },
      component: () => import('@/project/views/module/list')
    }
  ]
}
export default module
