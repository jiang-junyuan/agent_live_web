const login = {
  routes: [
    {
      path:'/',
      redirect:'/login'
    },
    {
      path: '/index',
      name: 'Welcome',
      meta:{
        breadcrumb:[
          {
            name:'首页',
            path:'/index'
          }
        ],
        isShowLeftSider:true,
        isShowHeader:true,
      },
      component: () => import( '@/project/views/Welcome.vue')
    },
    {
      path:'/login',
      name:'login',
      meta:{
        isShowHeader:false,
        isShowLeftSider:false,
      },
      component: () => import( '@/project/views/Login.vue'),
    },
    {
      path: '/login/agreement',
      name:'agreement',
      meta:{
        isShowHeader:false,
        isShowLeftSider:false,
      },
      component: () => import( '@/project/views/agreement/agreement'),
    },
    // {
    //   path: '/login/bind/:code',
    //   name:'bind',
    //   meta:{
    //     isShowHeader:false,
    //     isShowLeftSider:false,
    //   },
    //   component: () => import( '@/project/views/first-login/firstLogin'),
    // }
  ]
}
export default login;
