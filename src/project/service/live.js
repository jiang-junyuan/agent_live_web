import axios from '@/framework/http/axios'
const model = 'live'

let findWithMeUrl = `api/${model}/findWithMe`
let saveUrl = `api/${model}/save`
let updateUrl = `api/${model}/update`
let batchCancelUrl = `api/${model}/batchCancel`
let countWithMeUrl = `api/${model}/countWithMe`
let deleteUrl = `api/${model}/delete`
let getDetailsUrl = `api/${model}/get`
let findPlaybackLiveUrl = `api/${model}/findPlaybackLive`
let countPlaybackLiveUrl = `api/${model}/countPlaybackLive`

//查询/搜索直播间列表
export function findWithMe(param, callback) {
  axios.post(findWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询/搜索直播间列表
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询/搜索直播间列表
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//取消直播间
export function batchCancel(param, callback) {
  axios.post(batchCancelUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//统计直播间列表
export function countWithMe(param, callback) {
  axios.post(countWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//删除直播间
export function deleteLive(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 获取直播间详情
export function getDetails(param, callback) {
  axios.post(getDetailsUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 查询直播回放列表
export function findPlaybackLive(param, callback) {
  axios.post(findPlaybackLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 统计直播回放列表
export function countPlaybackLive(param, callback) {
  axios.post(countPlaybackLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

