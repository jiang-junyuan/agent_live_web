import axios from '@/framework/http/axios'
const model = 'mute'

let deleteUrl = `api/${model}/delete`
let saveUrl = `api/${model}/save`
let findUrl = `api/${model}/find`

//解除禁言
export function deleteMute(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//禁言
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//根据直播查询禁言列表
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

