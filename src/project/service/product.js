import axios from '@/framework/http/axios'
const model = 'product'

let countUrl = `api/${model}/count`
let findUrl = `api/${model}/find`
let findByLiveUrl = `api/${model}/findByLive`
let findByVideoUrl = `api/${model}/findByVideo`
let upUrl = `api/${model}/up`
let downUrl = `api/${model}/down`
let batchReverseUrl = `api/${model}/batchReverse`
let saveUrl = `api/${model}/save`
let batchDeleteUrl = `api/${model}/batchDelete`
let countByVideoUrl = `api/${model}/countByVideo`

// 统计外部商品
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 查询外部商品/搜索
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 根据直播间查询商品
export function findByLive(param, callback) {
  axios.post(findByLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 根据直播间查询商品
export function findByVideo(param, callback) {
  axios.post(findByVideoUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 上移
export function down(param, callback) {
  axios.post(downUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 下移
export function up(param, callback) {
  axios.post(upUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 批量上架/下架商品
export function batchReverse(param, callback) {
  axios.post(batchReverseUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 批量添加商品
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 批量移除商品
export function batchDelete(param, callback) {
  axios.post(batchDeleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
// 统计短视频查询商品
export function countByVideo(param, callback) {
  axios.post(countByVideoUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
