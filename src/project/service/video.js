import axios from '@/framework/http/axios'
const model = 'video'

let saveUrl = `api/${model}/save`
let findWithMeUrl = `api/${model}/findWithMe`
let countWithMeUrl = `api/${model}/countWithMe`
let getUrl = `api/${model}/get`
let batchDeletedUrl = `api/${model}/batchDeleted`
let batchReleaseUrl = `api/${model}/batchRelease`
let batchRemoveUrl = `api/${model}/batchRemove`
let updateUrl = `api/${model}/update`

//上传视频
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询短视频列表/搜索
export function findWithMe(param, callback) {
  axios.post(findWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//统计短视频列表/搜索
export function countWithMe(param, callback) {
  axios.post(countWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//获取视频详情
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量删除
export function batchDeleted(param, callback) {
  axios.post(batchDeletedUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量发布
export function batchRelease(param, callback) {
  axios.post(batchReleaseUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量下架
export function batchRemove(param, callback) {
  axios.post(batchRemoveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//编辑视频
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
