import axios from '@/framework/http/axios'
const model = 'module'

let saveUrl = `api/${model}/save`
let findByAccountTypeAndClientUrl = `api/${model}/findByAccountTypeAndClient`
let deleteUrl = `api/${model}/delete`
let removePermissionUrl = `api/${model}/removePermission`
let updateUrl = `api/${model}/update`
let findByRoleAndClientUrl = `api/${model}/findByRoleAndClient`
let findMyUrl = `api/${model}/findMy`
let addPermissionUrl = `api/${model}/addPermission`
let getUrl = `api/${model}/get`

//保存功能模块
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询指定类型的顶级菜单节点
export function findByAccountTypeAndClient(param, callback) {
  axios.post(findByAccountTypeAndClientUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//删除
export function del(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//删除权限
export function removePermission(param, callback) {
  axios.post(removePermissionUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//修改模块
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询指定角色在指定终端能访问的模块
export function findByRoleAndClient(param, callback) {
  axios.post(findByRoleAndClientUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//获取自己的有权限访问的根模块
export function findMy(param, callback) {
  axios.post(findMyUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//增加权限
export function addPermission(param, callback) {
  axios.post(addPermissionUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}


//获取功能模块
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
