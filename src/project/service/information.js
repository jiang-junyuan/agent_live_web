import axios from '@/framework/http/axios'
const model = 'information'

let searchUrl = `api/${model}/find`
let countUrl = `api/${model}/count`
let batchReverseUrl = `api/${model}/batchReverse`
let getUrl = `api/${model}/get`

//搜索
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//启用禁用
export function batchReverse(param, callback) {
  axios.post(batchReverseUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//获取
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
