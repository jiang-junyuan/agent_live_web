import axios from '@/framework/http/axios'
const model = 'store'
let getEffectedDateUrl = `api/${model}/getEffectedDate`

//首次登录绑定手机号
export function getEffectedDate(param, callback){
  axios.post(getEffectedDateUrl,param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
