import axios from '@/framework/http/axios'
const model = 'user'

let searchUrl = `api/${model}/findAllUser`
let countUrl = `api/${model}/countAllUser`

//查询全部用户
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计全部用户
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
