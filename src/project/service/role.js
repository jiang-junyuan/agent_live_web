import axios from '@/framework/http/axios'
const model = 'role'

let batchEnableUrl = `api/${model}/batchEnable`
let findByAccountIdAndAccountTypeAndAndOrganizationIdAndOrganizationUrl = `api/${model}/findByAccountIdAndAccountTypeAndAndOrganizationIdAndOrganization`
let batchDisableUrl = `api/${model}/batchDisable`
let findByAccountTypeUrl = `api/${model}/findByAccountType`
let batchDeleteUrl = `api/${model}/batchDelete`
let findByCodeUrl = `api/${model}/findByCode`
let updateModuleUrl = `api/${model}/updateModule`

//系统管理-角色
let searchUrl = `api/${model}/findByManagerWeb`
let countUrl = `api/${model}/countByManagerWeb`
let reverseUrl = `api/${model}/reverse`
let savePresetAgentRoleUrl = `api/${model}/savePresetAgentRole`
let findByPresetIsTrueUrl = `api/${model}/findByPresetIsTrue`
let getUrl = `api/${model}/get`
let enableUrl = `api/${model}/enable`
let updateByManagerWebUrl = `api/${model}/updateByManagerWeb`
let deleteByManagerWebUrl = `api/${model}/deleteByManagerWeb`
let updateUrl = `api/${model}/update`
let deleteUrl = `api/${model}/delete`
let updatePresetAgentRoleUrl = `api/${model}/updatePresetAgentRole`
let saveByManagerWebUrl = `api/${model}/saveByManagerWeb`

//批量启用角色
export function batchEnable(param, callback) {
  axios.post(batchEnableUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询指定机构下账号的所有角色
export function findByAccountIdAndAccountTypeAndAndOrganizationIdAndOrganization(param, callback) {
  axios.post(findByAccountIdAndAccountTypeAndAndOrganizationIdAndOrganizationUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量禁用角色
export function batchDisable(param, callback) {
  axios.post(batchDisableUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//根据账号类型查询角色
export function findByAccountType(param, callback) {
  axios.post(findByAccountTypeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量删除角色
export function batchDelete(param, callback) {
  axios.post(batchDeleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//搜索特定的角色
export function findByCode(param, callback) {
  axios.post(findByCodeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//更新角色的功能模块
export function updateModule(param, callback) {
  axios.post(updateModuleUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询角色
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计角色
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//启用/禁用角色权限
export function reverse(param, callback) {
  axios.post(reverseUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//新建预设角色权限
export function savePresetAgentRole(param, callback) {
  axios.post(savePresetAgentRoleUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询预设角色列表
export function findByPresetIsTrue(param, callback) {
  axios.post(findByPresetIsTrueUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//获取角色详情
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//启用禁用
export function enable(param, callback) {
  axios.post(enableUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//编辑角色
export function updateByManagerWeb(param, callback) {
  axios.post(updateByManagerWebUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//删除角色
export function deleteByManagerWeb(param, callback) {
  axios.post(deleteByManagerWebUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//编辑角色权限
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//删除角色权限
export function del(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//编辑角色权限
export function updatePresetAgentRole(param, callback) {
  axios.post(updatePresetAgentRoleUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//编辑角色权限
export function saveByManagerWeb(param, callback) {
  axios.post(saveByManagerWebUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
