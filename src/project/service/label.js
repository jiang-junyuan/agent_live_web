import axios from '@/framework/http/axios'
const model = 'label'

let findUrl = `api/${model}/find`

//查询标签
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
