import axios from '@/framework/http/axios'
const model = 'agent'
let updatePhoneUrl = `api/${model}/updatePhone`
let saveUrl = `api/${model}/save`
let reverseUrl = `api/${model}/reverse`
let getUrl = `api/${model}/get`
let countUrl = `api/${model}/count`
let updateUrl = `api/${model}/update`
let updateWithMeUrl = `api/${model}/updateWithMe`
let updateWechatIdUrl = `api/${model}/updateWechatId`
let searchUrl = `api/${model}/find`
let authenticateByOpenCodeUrl = `api/${model}/authenticateByOpenCode`
let sendCaptchaByTypeUrl = `api/${model}/sendCaptchaByType`
let deleteUrl = `api/${model}/delete`
let authenticateByUsernameUrl = `api/${model}/authenticateByUsername`
let updatePasswordWithMeUrl = `api/${model}/updatePasswordWithMe`
let authenticateByPhoneAndCaptchaUrl = `api/${model}/authenticateByPhoneAndCaptcha`
let updateRoleUrl = `api/${model}/updateRole`
let firstBindPhoneUrl = `api/${model}/firstBindPhone`
let validateByPhoneWithMeUrl = `api/${model}/validateByPhoneWithMe`
let  findWithMeUrl = `api/${model}/findWithMe`

//首次登录绑定手机号
export function firstBindPhone(param, callback){
  axios.post(firstBindPhoneUrl,param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      callback(data)
    }
  })
}
//短信登录
export function authenticateByPhoneAndCaptcha(param, callback){
  axios.post(authenticateByPhoneAndCaptchaUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//验证本人手机号验证码
export function validateByPhoneWithMe(param, callback){
  axios.post(validateByPhoneWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//手机号绑定
export function updatePhone(param, callback) {
  axios.post(updatePhoneUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//获取本人信息
export function findWithMe(param, callback) {
  axios.post(findWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}


//新建管理员
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//启用/禁用管理员
export function reverse(param, callback) {
  axios.post(reverseUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//获取管理员信息
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计管理员列表
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//编辑我的资料
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//编辑个人信息
export function updateWithMe(param, callback) {
  axios.post(updateWithMeUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//微信绑定
export function updateWechatId(param, callback) {
  axios.post(updateWechatIdUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查询管理员列表
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//微信扫码登录
export function authenticateByOpenCode(param, callback) {
  axios.post(authenticateByOpenCodeUrl, param).then(data => {
    callback(data)
  })
}
//发送手机验证码
export function sendCaptchaByType(param, callback) {
  axios.post(sendCaptchaByTypeUrl, param).then(data => {
    callback(data)
  })
}
//删除管理员
export function del(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    callback(data)
  })
}
//账号密码登录
export function authenticateByUsername(param, callback) {
  axios.post(authenticateByUsernameUrl, param).then(data => {
    console.log(data)
    callback(data)
  })
}
//修改密码
export function updatePassword(param, callback) {
  axios.post(updatePasswordWithMeUrl, param).then(data => {
    // console.log(data)
    callback(data)
  })
}

//更新角色
export function updateRole(param, callback) {
  axios.post(updateRoleUrl, param).then(data => {
    callback(data)
  })
}
