import axios from '@/framework/http/axios'
const model = 'resource'

let searchUrl = `api/${model}/find`
let countUrl = `api/${model}/count`
let findByModuleIdUrl = `api/${model}/findByModuleId`

//查询权限
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//统计权限
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询指定模块对应的权限
export function findByModuleId(param, callback) {
  axios.post(findByModuleIdUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
