import axios from '@/framework/http/axios'
const model = 'region'
let getRegionUrl = `api/${model}/find`

//首次登录绑定手机号
export function getRegion(param, callback){
  axios.post(getRegionUrl,param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
