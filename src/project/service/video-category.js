import axios from '@/framework/http/axios'
const model = 'videoCategory'

let findByEnabledIsTrueUrl = `api/${model}/findByEnabledIsTrue`

//查询频道
export function findByEnabledIsTrue(param, callback) {
  axios.post(findByEnabledIsTrueUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
