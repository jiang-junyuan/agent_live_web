import axios from '@/framework/http/axios'
const model = 'barrage'

let saveUrl = `api/${model}/save`
let findByLiveUrl = `api/${model}/findByLive`
let findWithAgentUrl = `api/${model}/findWithAgent`


//保存弹幕/新建评论内容
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//保存弹幕/新建评论内容
export function findByLive(param, callback) {
  axios.post(findByLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//保存弹幕/新建评论内容
export function findWithAgent(param, callback) {
  axios.post(findWithAgentUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
