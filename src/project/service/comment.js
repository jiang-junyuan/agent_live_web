import axios from '@/framework/http/axios'
const model = 'comment'

let countByVideoOrLiveUrl = `api/${model}/countByVideoOrLive`
let findByVideoOrLiveUrl = `api/${model}/findByVideoOrLive`

//根据短视频/直播统计评论
export function countByVideoOrLive(param, callback) {
  axios.post(countByVideoOrLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//根据短视频/直播查询评论
export function findByVideoOrLive(param, callback) {
  axios.post(findByVideoOrLiveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}


