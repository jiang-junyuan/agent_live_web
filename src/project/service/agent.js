import axios from '@/framework/http/axios'
const model = 'agent'

let saveUrl = `api/${model}/savePresetAgent`
let searchUrl = `api/${model}/findPresetAgent`
let updateUrl = `api/${model}/updatePresetAgent`
let getUrl = `api/${model}/get`
let enableUrl = `api/${model}/reversePresetAgent`
let deleteUrl = `api/${model}/deletePresetAgent`
let findByStoreUrl = `api/${model}/findByStore`
let findMasterUrl = `api/${model}/findMaster`

//新建预设账号
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//获取预设账号
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//编辑预设账号
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查看预设账号
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//启用禁用预设账号
export function enable(param, callback) {
  axios.post(enableUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//删除预设账号
export function del(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//根据店铺查询账号
export function findByStore(param, callback) {
  axios.post(findByStoreUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查询主账号的实名信息
export function findMaster(param, callback) {
  axios.post(findMasterUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
