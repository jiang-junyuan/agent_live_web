import axios from '@/framework/http/axios'
const model = 'notification'

let searchUrl = `api/${model}/find`
let countUrl = `api/${model}/count`
let batchCancelUrl = `api/${model}/batchCancel`
let batchSaveUrl = `api/${model}/batchSave`

//查询指定通知包的通知列表
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计指定通知包的通知列表
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//批量取消
export function batchCancel(param, callback) {
  axios.post(batchCancelUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//添加推送用户
export function batchSave(param, callback) {
  axios.post(batchSaveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
