import axios from '@/framework/http/axios'
const model = 'anchor'

let updateUrl = `api/${model}/update`
let findUrl = `api/${model}/find`
let getCountUrl = `api/${model}/count`
let saveUrl = `api/${model}/save`
let batchDeleteUrl = `api/${model}/batchDelete`


//编辑主播
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//查询主播
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//统计主播数量
export function getCount(param, callback) {
  axios.post(getCountUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//新建主播
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//批量删除
export function batchDelete(param, callback) {
  axios.post(batchDeleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

