import axios from '@/framework/http/axios'
const model = 'agentRole'

let saveUrl = `api/${model}/savePresetAgentRole`
let searchUrl = `api/${model}/findByPresetIsTrue`
let countUrl = `api/${model}/count`
let findByPresetIsTrueUrl = `api/${model}/findByPresetIsTrue`
let getUrl = `api/${model}/get`
let updateUrl = `api/${model}/update`
let enableUrl = `api/${model}/enable`
let deleteUrl = `api/${model}/delete`


//新建角色权限
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查询
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查询预设角色列表
export function findByPresetIsTrue(param, callback) {
  axios.post(findByPresetIsTrueUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//查看详情
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//更新
export function update(param, callback) {
  axios.post(updateUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//启用禁用
export function enable(param, callback) {
  axios.post(enableUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//删除
export function del(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
