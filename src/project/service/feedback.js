import axios from '@/framework/http/axios'
const model = 'feedback'

let searchUrl = `api/${model}/find`
let countUrl = `api/${model}/count`
let getUrl = `api/${model}/get`
let batchDeleteUrl = `api/${model}/batchDelete`
let batchResolveUrl = `api/${model}/batchResolve`

//查询所有反馈
export function search(param, callback) {
  axios.post(searchUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//统计所有反馈
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//获取意见反馈详情
export function get(param, callback) {
  axios.post(getUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//删除
export function batchDelete(param, callback) {
  axios.post(batchDeleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
//标志已处理
export function batchResolve(param, callback) {
  axios.post(batchResolveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
