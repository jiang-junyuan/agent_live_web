import axios from '@/framework/http/axios'
const model = 'block'

let countUrl = `api/${model}/count`
let findUrl = `api/${model}/find`
let deleteUrl = `api/${model}/delete`
let saveUrl = `api/${model}/save`
let saveByUserUrl = `api/${model}/saveByUser`

// 统计黑名单
export function count(param, callback) {
  axios.post(countUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 查询黑名单
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 移除
export function deleteBlack(param, callback) {
  axios.post(deleteUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 拉黑
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

// 拉入黑名单
export function saveByUser(param, callback) {
  axios.post(saveByUserUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}


