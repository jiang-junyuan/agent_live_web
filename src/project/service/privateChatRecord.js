import axios from '@/framework/http/axios'
const model = 'privateChatRecord'

let saveUrl = `api/${model}/save`
let findUrl = `api/${model}/find`

//私信记录信息
export function save(param, callback) {
  axios.post(saveUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}

//根据直播查询私信记录信息
export function find(param, callback) {
  axios.post(findUrl, param).then(data => {
    if (data !== undefined && data !== '' && data !== null) {
      // callback when data is exist
      callback(data)
    }
  })
}
