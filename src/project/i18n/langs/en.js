import enLocale from 'element-ui/lib/locale/lang/en'
const en = {
  project: {
    projectName: 'Liangjian Cloud Health APP management background',
    username: 'username',
    password: 'password',
    signIn: 'signIn',
    index: 'Index',
    updatePassword: 'Change password',
    logout: 'Log out',
    search: 'Search',
    reset: 'Reset',
    new: 'New',
    base: 'Basic information',
    more: 'More operations',
    启用: 'enable',
    禁用: 'disable',
    编辑: 'edit',
    上架: 'Shelve',
    下架: 'Unshelve',
    审核: 'Review',
    删除: 'Delete',
    基本信息: 'Base information',
    编辑信息: 'Edit information',
    enable: 'enable',
    disable: 'disable',
    edit: 'edit',
    view: 'view',
    delete: 'delete',
    confirm: 'confirm',
    cancel: 'cancel',
    start: 'Start date',
    end: 'End date',
    select: 'Please Select',
    upload: 'Please upload',
    empty: 'Not empty',
    editSuccess: 'Edit success',
    createSuccess: 'Create success',
    deleteSuccess: 'Delete success',
    prev: 'prev',
    next: 'next',
    选择: 'selected',
    complete: 'Please complete the required fields on the form',
    review: 'Review the success',
    deleteOne: 'Are you sure to delete this data?',
    tip: 'Tips',
    cancelOper: 'Cancelled operation',
    lessOne: 'Select at least one piece of data',
    batchDelete: 'Are you sure to delete the selected data?',
    updatePass: 'Change password',
    originPass: 'Original password',
    newPass: 'New password',
    confirmPass: 'Confirm new password',
    passTips: 'The passwords are inconsistent. Please re-enter them',
    passAgain: 'Please enter your new password again',
    logout: 'Exit the success',
    originError: 'The original password is wrong. Please re-enter it',
    changeSuccess: 'Password changed successfully, please log in again!'
  },
  menu: {
    系统管理: 'System management',
    管理员管理: 'Admin management',
    角色管理: 'Role management',
    APP管理: 'APP management',
    模块管理: 'Module management',
    权限管理: 'Permission management',
    系统操作日志: 'System operation log',
    // ====================
    用户管理: 'User management',
    用户列表: 'List of users',
    医护人员列表: 'List of medical staff',
    // ====================
    短视频管理: 'Short video management',
    视频管理: 'Video management',
    视频审核: 'Video review',
    科室图标管理: 'Department icon Management',
    // ======================
    直播管理: 'On the management',
    直播统计: 'Live statistics',
    直播礼物管理: 'Live Gift Management',
    // ======================
    热搜管理: 'Hot search management',
    // ======================
    热门管理: 'Top management',
    // ======================
    话题管理: 'Topic management',
    // ======================
    音乐库管理: 'Music Library Management',
    // ======================
    评论管理: 'Comment Management',
    // ======================
    内容管理: 'Content management',
    敏感词管理: 'Sensitive word management',
    广告管理: 'Advertising management',
    系统文章管理: 'System article management',
    // ======================
    消息管理: 'Message management',
    // ======================
    举报管理: 'Report management',
    举报类型管理: 'Report type management',
    // ======================
    财务管理: 'Financial management',
    平台流水: 'Platform of the water',
    充值套餐管理: 'Recharge package management',
    // ======================
    水印管理: 'Watermark management',
    // ======================
    语言管理: 'Language management',
    语言设置: 'Language Settings'
    // ======================
  },
  breadcrumb: {
    首页: 'Home page',
    管理员管理: 'Admin management',
    管理员详情: 'Administrator details',
    角色管理: 'Role management',
    角色详情: 'Role details',
    APP管理: 'APP management',
    模块管理: 'Module management',
    权限管理: 'Permission management',
    系统操作日志: 'System operation log',
    用户列表: 'List of users',
    用户详情: 'The user details',
    医护人员列表: 'List of medical staff',
    医护人员详情: 'Details of medical staff',
    视频管理: 'Video management',
    视频详情: 'The video details',
    视频审核: 'Video review',
    视频审核详情: 'Video review details',
    科室图标管理: 'Department icon Management',
    直播管理: 'On the management',
    直播统计: 'Live statistics',
    直播详情: 'Live details',
    直播回放详情: 'Live playback details',
    直播礼物管理: 'Live Gift Management',
    热搜管理: 'Hot search management',
    热门管理: 'Top management',
    话题管理: 'Topic management',
    话题详情: 'Topic details',
    音乐库管理: 'Music Library Management',
    评论管理: 'Comment Management',
    评论详情: 'Comment details',
    敏感词管理: 'Sensitive word management',
    广告管理: 'Advertising management',
    广告详情: 'Advertising details',
    系统文章: 'System article',
    消息管理: 'Message management',
    消息详情: 'Message details',
    举报管理: 'Report management',
    举报类型管理: 'Report type management',
    平台流水: 'Platform of the water',
    充值套餐管理: 'Recharge package management',
    水印管理: 'Watermark management',
    语言设置: 'Language Settings',
    达人列表: 'Expert list',
    达人详情: 'Expert details',
  },
  search: {
    loading: 'loading.....',
    账号: 'Username',
    姓名: 'Realname',
    状态: 'Status',
    角色名称: 'Role name',
    手机号: 'Phone',
    功能模块: 'Function module',
    操作时间: 'Operating time',
    昵称: 'Nickname',
    地区: 'District',
    时间: 'Time',
    日期: 'Time',
    性别: 'Gender',
    年龄范围: 'Age range',
    标签: 'Label',
    医院: 'Hospital',
    科室: 'Department',
    职称: 'Title',
    视频标题: 'Video name',
    房间号: 'House number',
    关键词: 'Keyword',
    话题名称: 'Topic name',
    歌名: 'Song name',
    评论者ID: 'Commentators ID',
    被评论者ID: 'Commenter ID',
    评论时间: 'Comment time',
    评论内容: 'Comment content',
    敏感词: 'Sensitive words',
    标题: 'title',
    排序数字: 'position',
    位置: 'location',
    支付方式: 'Payment method'
  },
  manager: {
    username: 'Username',
    realname: 'Realname',
    password: 'Password',
    phone: 'Phone',
    status: 'Status',
    avatar: 'Avatar',
    remark: 'Remark',
    角色设置: 'Character set',
    添加角色: 'Add roles',
    删除角色: 'Delete roles',
    角色名称: 'Role name',
    角色备注: 'Role remark',
    phoneTip: 'The format of mobile phone number is wrong, please check and re-enter',
    usernameTip: 'It must begin with a letter',
    操作: 'Operation',
    权限数量: 'Access number',
    上次登录地点: 'Last login place',
    上次登录时间: 'Last login time'
  },
  role: {
    角色: 'Role',
    备注: 'Remark',
    权限数量: 'Permission number',
    权限: 'Permission',
    状态: 'Status',
    角色名称: 'Role name',
    权限设置: 'Permissions',
    确定离开: 'Determined to leave',
    返回确认: 'Return to confirm',
    提示: 'Tips',
    leaveTips:
      'Before leaving this page, if you have control rights checked, please make sure that you click the confirm update button before leaving!',
    tips: 'Please remember to click this button after updating the permissions!',
    confirm: 'confirm',
    基本信息: 'Basic information',
    编辑信息: 'Edit information'
  },
  version: {
    用户端: 'Client side',
    医生端: 'Doctor side',
    Android_手机端: 'Android_mobiles',
    引导页: 'Guide page',
    版本号: 'Version number',
    版本描述: 'Version description',
    强制更新: 'Optional',
    创建时间: 'CreateTime',
    系统: 'System',
    版本名称: 'Version name',
    链接地址: 'Link address',
    下载地址: 'Download address',
    tips: 'You can only choose one of the download and link addresses',
    版本信息: 'Version information',
    编辑版本信息: 'Edit version information',
    序号: 'Index',
    图片: 'Image',
    操作: 'Operation',
    请上传图片: 'Please upload image'
  },
  operation: {
    账号: 'Username',
    角色: 'Role',
    姓名: 'Realname',
    手机号: 'Phone',
    操作模块: 'Operation module',
    操作内容: 'Operation content',
    操作内容: 'Operation time'
  },
  user: {
    集团ID: 'Group ID',
    unionId: 'Union ID',
    用户ID: 'User ID',
    昵称: 'Nickname',
    手机号: 'Phone',
    年龄: 'Age',
    性别: 'Gender',
    地区: 'District',
    标签: 'Label',
    最后活跃时间: 'Last active time',
    状态: 'Status',
    操作: 'Operation',
    头像: 'Avatar',
    注册时间: 'Registration time',
    发生时间: 'Create time',
    启用状态: 'Status',
    账号信息: 'Account information',
    我的赏金: 'My bounty',
    累计充值总额: 'Accumulative total recharge',
    流水号: 'Serial number',
    类型: 'Type',
    事由: 'Reason',
    流水金额: 'Amount',
    支付方式: 'Payment Method',
    创建时间: 'Create time',
    医生列表: 'Doctor list',
    达人列表: 'Expert list',
    黑名单: 'Blacklist',
    医生ID: 'Doctor ID',
    姓名: 'Realname',
    职称: 'Title',
    科室: 'Department',
    身份证号: 'Id number',
    机构: 'Organization',
    角色: 'Role',
    简介: 'Description',
    被禁止内容: 'Prohibited Content',
    医院: 'Hospital',
    tips: 'It is recommended to upload 200*200 pictures, and upload one local picture at most'
  },
  video: {
    封面图: 'Cover',
    姓名: 'Realname',
    视频标题: 'Video name',
    视频: 'video',
    地址: 'path',
    话题名称: 'Topic name',
    科室: 'Department',
    二级标签: 'Label',
    标签: 'Label',
    位置: 'Location',
    时长: 'Duration',
    视频大小: 'Video size',
    真实点赞量: 'ActualPraiseQuantity',
    真实播放量: 'ActualPlayQuantity',
    真实评论量: 'ActualCommentQuantity',
    真实分享量: 'ActualShareQuantity',
    设置点赞量: 'VirtualPraiseQuantity',
    设置评论量: 'VirtualCommentQuantity',
    设置播放量: 'VirtualPlayQuantity',
    设置分享量: 'VirtualShareQuantity',
    点赞量: 'Praise quantity',
    播放量: 'Play quantity',
    评论量: 'Comment quantity',
    分享量: 'Share quantity',
    发布时间: 'Release time',
    状态: 'Status',
    操作: 'Operation',
    修改标签: 'Update label',
    设置数据: 'SetData',
    不通过: 'No pass',
    通过: 'Pass',
    选择医生: 'Choose a doctor',
    tips: 'Support extension: .mp4',
    视频详情: 'Video details',
    添加标签: 'Add tags',
    LabelTips: 'Please select at least one label',
    nomore: 'No more tags',
    文字反馈: 'Written feedback',
    拒绝原因: 'Refusal cause',
    医院: 'Hospital name'
  },
  department: {
    图标: 'Icon',
    科室: 'Department',
    位置: 'Location',
    操作: 'Operation',
    选择图标: 'Select icon ',
    tips: 'Please select icon'
  },
  language: {
    语言信息: 'Language information',
    当前语言: 'Current language',
    设置: 'Setting',
    lang: 'language'
  },
  waterMark: {
    word: 'Word',
    image: 'Image',
    watermark: 'Watermark',
    time: 'Create time',
    status: 'Status'
  },
  financial: {
    用户余额汇总: 'Summary of user balance',
    充值金额汇总: 'Summary of recharge amount',
    消费金额汇总: 'Summary of consumption amount',
    流水号: 'Serial number',
    用户ID: 'User ID',
    昵称: 'Nickname',
    类型: 'Type',
    事由: 'Reason',
    流水金额: 'Amount',
    支付方式: 'Payment type',
    发生时间: 'Create time',
    序号: 'Index',
    充值金额: 'Top-up amount',
    赏金: 'Reward',
    操作: 'Operation',
    请输入充值金额: 'Please enter the recharge amount',
    只允许输入正数: 'Only positive numbers are allowed',
    请输入赏金: 'Please enter the reward money',
    最多只能输入小数点后两位: 'You can only enter two decimal places at most'
  },
  comment: {
    序号: 'Index',
    评论者ID: 'Commentators ID',
    手机号: 'Phone',
    被评论者ID: 'Commenter ID',
    视频标题: 'Video name',
    评论内容: 'Comment content',
    评论时间: 'Comment time',
    状态: 'Status',
    操作: 'Operation',
    评论详情: 'Comment details'
  },
  music: {
    封面图: 'Cover',
    歌名: 'Song name',
    歌手: 'Singer',
    时长: 'Duration',
    使用次数: 'Usage Counter',
    状态: 'Status',
    歌曲: 'Path'
  },
  report: {
    被举报视频: 'Reported video',
    被举报直播: 'Reported live',
    内容编号: 'Content number',
    举报用户昵称: 'Report user nickname',
    被举报医生姓名: 'Reported doctor',
    房间号: 'House number',
    举报类型: 'Report type',
    举报理由: 'Report reason',
    举报时间: 'Report time',
    状态: 'Status',
    操作: 'Operation',
    视频标题: 'Video name',
    审核: 'Review',
    审核反馈: 'Review the feedback',
    文字反馈: 'Written feedback',
    反馈内容: 'Info'
  },
  topic: {
    话题名称: 'Topic name',
    当前视频数量: 'Current video quantity',
    虚假视频数量: 'Virtual video quantity',
    操作: 'Operation',
    视频列表: 'Video list',
    序号: 'Index',
    姓名: 'Realname',
    标题: 'Title',
    真实点赞量: 'Actual praise quantity'
  },
  hot: {
    热门活动: 'Popular activities',
    热门直播: 'Popular live',
    封面图: 'Cover',
    姓名: 'Realname',
    房间号: 'House number',
    医院: 'Hospital',
    位置: 'Location',
    科室: 'Department',
    直播主题: 'Live topic',
    状态: 'Status',
    操作: 'Operation',
    链接: 'Link',
    selectHouse: 'Select studio (single)',
    选择: 'selected',
    热搜管理: 'Hot search management',
    推荐科室: 'Recommend department',
    搜索框: 'The search box',
    关键词: 'Keyword',
    链接地址: 'Link',
    排序数字: 'Position',
    点击次数: 'Clicks',
    创建时间: 'Create time',
    请选择科室: 'Please select department',
    手机号: 'Phone'
  },
  content: {
    敏感词: 'Sensitive words',
    状态: 'Status',
    操作: 'Operation',
    文章名称: 'Article name',
    链接地址: 'Link',
    广告标题: 'Advertisement title',
    位置: 'Location',
    广告内容: 'Advertisement content',
    排序数值: 'Posistion',
    广告管理: 'Advertising management',
    tips: 'It is recommended to upload 200*200 pictures, and upload one local picture at most',
    first: 'Steps 1',
    second: 'Steps 2',
    广告图片: 'Advertising images',
    广告类型: 'Advertising type',
    外部链接: 'External links',
    排序数字: 'Position'
  },
  live: {
    预热: 'Preheating',
    直播中: 'Living',
    回放: 'Replay',
    房间号: 'House number',
    姓名: 'Realname',
    手机号: 'Phone',
    医院: 'Hospital',
    科室: 'Department',
    直播主题: 'Live topic',
    创建时间: 'Create time',
    开播时间: 'Open time',
    结束时间: 'End time',
    状态: 'Status',
    操作: 'Operation',
    图片: 'Image',
    直播画面: 'Live coverage',
    直播回放: 'Live playback',
    直播数据: 'Live data',
    当前直播数据: 'Current live data',
    截止直播数据: 'Cut-off live data',
    数值: 'Numeric value',
    编辑峰值观看人数: 'Edit peak viewership',
    实际当前峰值观看人数: 'Actual current peak viewership',
    当前观看人数: 'Current number of viewers',
    当前评论人数: 'Current number of comments',
    当前评论量: 'Current comment volume',
    当前点赞人数: 'Current thumb up population',
    当前点赞量: 'Current thumb up quantity',
    当前粉丝打赏数: 'Current number of fan hits',
    当前粉丝打赏金币: 'Current fans reward gold COINS',
    当前非粉丝打赏数: 'Current number of non-fan hits',
    当前非粉丝打赏金币: 'Current non-fan reward gold COINS',
    峰值观看人数: 'Peak viewership',
    观看人数: 'Views number',
    评论人数: 'Number of comments',
    评论量: 'Comment on the amount',
    点赞人数: 'The number of thumb up',
    点赞量: 'Thumb up quantity',
    粉丝打赏数: 'Fans reward them',
    粉丝打赏金币: 'Fans reward gold COINS',
    非粉丝打赏数: 'Non-fans reward them',
    非粉丝打赏金币: 'Non-fans reward gold COINS',
    序号: 'Index',
    名称: 'Name',
    价格: 'Price',
    编辑礼物: 'Edit gift',
    新增礼物: 'New gift',
    标识名: 'Identification name',
    订阅人数: 'Subscriber Quantity',
    礼物数量: 'Gifts Quantity'
  },
  message: {
    系统推送: 'System to push',
    短信推送: 'SMS push',
    通知栏推送: 'Notification push',
    推送内容: 'Push content',
    创建时间: 'Create time',
    消息标题: 'Message header',
    信息详情: 'Message details',
    链接地址: 'Link',
    未推送: 'Not push',
    已推送: 'Pushed',
    已取消: 'canceled ',
    推送: 'Push',
    选择推送日期: 'Select push date',
    编辑推送内容: 'Edit push content',
    编辑详情链接: 'Edit details link',
    选择推送对象: 'Select push object',
    推送日期: 'Push time',
    video: 'Select Video (Single option)',
    live: 'Select Live (Single option)',
    消息内容: 'Message Content',
    类型: 'Type',
    推送对象: 'Push object',
    全部用户: 'All users',
    指定用户: 'Named user',
    用户列表: 'Users list',
    医生列表: 'Doctors list',
    短信详情: 'SMS details',
    选择用户: 'Select the user',
    立即推送: 'Immediately push',
    定时推送: 'Timed push',
    手机号: 'Phone',
    昵称: 'Nickname',
    性别: 'Gender',
    标签: 'Label',
    全部恢复: 'Fully recovery',
    批量恢复: 'Batch recovery',
    操作: 'Operation',
    恢复: 'Recover',
    recover: 'Are you sure to restore the selected record?',
    恢复成功: 'Restore success',
    全部恢复成功: 'Full recovery',
    已取消恢复: 'Canceled recovery',
    添加推送人: 'Add pusher',
    全部取消: 'Cancel all',
    批量取消: 'Batch cancelled',
    推送时间: 'Push time',
    取消成功: 'Cancel Successful',
    cancel: 'Are you sure to cancel the selected record?',
    已取消删除: 'Undeleted'
  },
  ...enLocale
}
export default en
