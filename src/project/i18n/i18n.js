
import Vue from 'vue';
import ElementLocale from 'element-ui/lib/locale'
import VueI18n from 'vue-i18n';
import messages from './langs';

Vue.use(VueI18n);

const i18n = new VueI18n({    //从localStorage中取，没有就默认的中文
  locale: localStorage.language || 'zh',
  messages,
  silentTranslationWarn: true,
})

ElementLocale.i18n((key, value) => i18n.t(key, value))
export default i18n