// 从v2.11.2起，SDK 支持了 WebSocket，推荐接入；v2.10.2及以下版本，使用 HTTP
import TIM from 'tim-js-sdk';
import TIMUploadPlugin from 'tim-upload-plugin';
import store from "../store";

let options = {
  SDKAppID:1400695831// 接入时需要将0替换为您的即时通信 IM 应用的 SDKAppID
};
// 创建 SDK 实例，`TIM.create()`方法对于同一个 `SDKAppID` 只会返回同一份实例
let tim = TIM.create(options); // SDK 实例通常用 tim 表示

// 设置 SDK 日志输出级别，详细分级请参见 <a href="https://web.sdk.qcloud.com/im/doc/zh-cn//SDK.html#setLogLevel">setLogLevel 接口的说明</a>
tim.setLogLevel(0); // 普通级别，日志量较多，接入时建议使用
// tim.setLogLevel(1); // release 级别，SDK 输出关键信息，生产环境时建议使用

// 注册腾讯云即时通信 IM 上传插件
tim.registerPlugin({'tim-upload-plugin': TIMUploadPlugin});


//接收
let receiveMessage = (event) => {
  // 收到推送的单聊、群聊、群提示、群系统通知的新消息，可通过遍历 event.data 获取消息列表数据并渲染到页面
  // event.name - TIM.EVENT.MESSAGE_RECEIVED
  // event.data - 存储 Message 对象的数组 - [Message]
  // console.log('监听里收到消息',message.flow)//in 为收到的消息  out 为发出去的消息

  let message = event.data[0]

  console.log('收到了')

  //群聊消息
  if (message.conversationType === 'GROUP') {
    // console.log('收到群聊的消息')
    //IM的群聊系统消息
    if (message.from === '@TIM#SYSTEM') {
      // console.log('群聊的系统消息')
    }
    //收到弹幕消息
    else {
      console.log('收到群聊的消息', message.nick, message.payload, message.to)
      // store.commit('updateGroupChatList', message)
      //添加一个属性 判断是 收到的消息 还是自己发送出去的消息（store中的updateMsgList中的判断用到）
      message.sendByOtherUser=true
      store.commit('updateMsgList', message)
      store.commit('changeStateUnRead', true)
    }
  }else{ //私聊
    console.log('收到私聊的消息', message.nick, message.payload, message.to, message.from)
    //添加一个属性 判断是 收到的消息 还是自己发送出去的消息（store中的updateMsgList中的判断用到）
    message.sendByOtherUser=true
    store.commit('updateMsgList', message)
    store.commit('changeStateUnRead', true)
  }

}

tim.on(TIM.EVENT.MESSAGE_RECEIVED, receiveMessage);

//会话列表
//监听好友列表发生变化
let onConversationListUpdated=(event)=>{
  store.commit('updateUserList')
}
//用户列表的监听 好友列表
tim.on(TIM.EVENT.CONVERSATION_LIST_UPDATED, onConversationListUpdated);

let errorFunction=(event)=>{
  // 收到 SDK 发生错误通知，可以获取错误码和错误信息
  // event.name - TIM.EVENT.ERROR
  // event.data.code - 错误码
  // event.data.message - 错误信息
  // console.log('腾讯云错误信息',event.data.message,)
}
tim.on(TIM.EVENT.ERROR, errorFunction );

let onSdkReady = function (event) {
  // let message = tim.createTextMessage({ to: store.state.currentChatId+'', conversationType: 'C2C', payload: { text: 'hallo' }});
  // tim.sendMessage(message);
  // console.log('直播间id：'+store.state.liveId)
  // store.dispatch('getMessageList', 'GROUP' + store.state.liveId + '')//获取聊天内容列表

  //获取聊天内容列表
  store.dispatch('getMessageList', 'GROUP' + '402703536720445440' + '')

  //加入群聊
  let options = {
    groupID: store.state.currentChatId,
    type: TIM.TYPES.GRP_AVCHATROOM
  }
  tim.joinGroup(options);
  let promise = tim.joinGroup(options);
  promise.then(function(imResponse) {
    switch (imResponse.data.status) {
      case TIM.TYPES.JOIN_STATUS_WAIT_APPROVAL: // 等待管理员同意
        console.log('等待管理员同意')
        break;
      case TIM.TYPES.JOIN_STATUS_SUCCESS: // 加群成功
        console.log('加入的群组资料',imResponse.data.group); // 加入的群组资料
        console.log('加群成功')
        break;
      case TIM.TYPES.JOIN_STATUS_ALREADY_IN_GROUP: // 已经在群中
        console.log('已经在群中')
        break;
      default:
        break;
    }
  }).catch(function(imError){
    console.warn('joinGroup error:', imError); // 申请加群失败的相关信息
  });

  //获取会话列表
  store.commit('updateUserList')
};
tim.on(TIM.EVENT.SDK_READY, onSdkReady);


let kickedOut=(event) =>{
  // 收到被踢下线通知
  console.log('腾讯云收到被踢下线通知,请重新登录')
  // event.name - TIM.EVENT.KICKED_OUT
  // event.data.type - 被踢下线的原因，例如 :
  //   - TIM.TYPES.KICKED_OUT_MULT_ACCOUNT 多实例登录被踢
  //   - TIM.TYPES.KICKED_OUT_MULT_DEVICE 多终端登录被踢
  //   - TIM.TYPES.KICKED_OUT_USERSIG_EXPIRED 签名过期被踢（v2.4.0起支持）。
}
tim.on(TIM.EVENT.KICKED_OUT, kickedOut);



export default tim;
