export function throttle(fn,wait){
  let timer = null
  return function(){
    let context = this
    let args = [...arguments]

    if(!timer){
      fn.apply(context,args)
      timer = setTimeout(() => {
        timer = null
      },wait)
    }
  }
}


