/*
 * @Author: your name
 * @Date: 2020-09-15 09:15:12
 * @LastEditTime: 2020-11-25 16:40:26
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \亮健云总后台\src\framework\http\axios.js
 */
import axios from 'axios'
import JSONBig from 'json-bigint'
import {Notification} from 'element-ui'
import router from '@/project/router'
// create axios instance
let instance = null


if (process.env.NODE_ENV === 'development') {
  instance = axios.create({
    baseURL: '/' ,// api context
    transformResponse:[function (data){
      // console.log("============data=================",data)
      if (typeof data === 'string') {
        try {
          data = JSONBig.parse(data);
        } catch (e) { /* Ignore */ }
      }
      return data;
    }]
  })
} else {
  // 生产环境下
  instance = axios.create({
    baseURL: '/', // api context
    transformResponse:[function (data){
      // console.log("============data=================",data)
      if (typeof data === 'string') {
        try {
          data = JSONBig.parse(data)
        } catch (e) { /* Ignore */ }
      }
      return data;
    }]
  })
}

instance.defaults.timeout = 50000 // timeout

// request interceptors
instance.interceptors.request.use(
  // request => {
  //   return request;
  // },
  config => {
    if (localStorage.getItem('agent_live_web_token')) {
      config.headers['Access-Token'] = localStorage.getItem('agent_live_web_token')
      config.headers['Content-Type'] = 'application/json'
    }
    return config
  },
  error => {
    // Do something with request error
    console.log('request', error)
    Notification({
      title: '错误',
      message: '这是一条错误的提示消息',
      type: 'error'
    })
    Promise.reject(error)
  }
)

// response interceptors response data format {code: 200, data: {}} or {code: 400, message: "error"}
instance.interceptors.response.use(response => {
    // console.log(response,"=========response===========")
    if (response.status == 200) {
      return response.data
    } else if (response.status == 401) {
      Notification({
        title: '警告',
        message: `已在别处登录或者token失效，请重新登录`,
        type: 'warning'
      })
      router.push('/login')
    } else if (response.status == 204) {
      return response.status
    } else {
      Notification({
        title: '警告',
        message: response.data.message,
        type: 'warning'
      })
    }
  }, error => {
    if(error.response.data.code === '500000') {
      Notification({
        title: '警告',
        message: error.response.data.message,
        type: 'warning'
      })
    } else {
      if(error.response.data.message === 'Token expired or does not exist') {
        Notification({
          title: '警告',
          message: 'token失效或者在别处登录，请重新登录',
          type: 'warning'
        })
        router.push({path: '/login'})
      } else {
        sessionStorage.setItem('error',error.response.data.message)
        Notification({
        title: '警告',
        message: error.response.data.message,
        type: 'warning'
      })
      }
      //500错误

    }
    // console.log(error, "===========error============")
  }
)

export default instance
