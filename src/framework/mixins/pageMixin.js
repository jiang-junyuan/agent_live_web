
// 分页参数以及分页方法的抽调代码
export default {
  data() {
    return {
      page: 1,
      pageSize: 10,
      total: 0,
      selectList: []
    }
  },
  methods: {
    handleCurrentChange(val) {
      this.page = val
      this.listUpdate()
    },

    handleSizeChange(pageSize) {
      this.pageSize = pageSize
      this.listUpdate()
    },

    handleSelectionChange(val) {
      this.selectList = val
    }
  }
}
