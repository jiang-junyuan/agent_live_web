/**
 * @author: cwlojako
 * @Date: 2020-09-10 11:10:46
 * @description: 启禁用的抽调方法
 */

const options = {
  batchChange: null,
  singleChange: null,
  singleChangeCircle: null
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 11:15:24
 * @param: 参数1： 方法名
 * @param: 参数2： 启用接口
 * @param: 参数3： 禁用接口
 * return 返回methods混入到页面中
 */
export default (optionKeys, enableFunc, disableFunc) => {
  if (enableFunc && disableFunc) {
    // 如果后端启禁用接口是分开的， 也即是是2个接口
    options.batchChange = bindBatchChange(enableFunc, disableFunc)
    options.singleChange = bindSingleChange(enableFunc, disableFunc)
    options.singleChangeCircle = bindSingleChangeCircle(enableFunc, disableFunc)
  } else if (enableFunc && !disableFunc) {
    // 如果后端启禁用是统一一个接口
    options.batchChange = bindBatchChange_ONE(enableFunc)
    options.singleChange = bindSingleChange_ONE(enableFunc)

    // 处理其他不是启禁用的接口，但传的参数一致，参数名不一样而已
    if (optionKeys[0] !== 'batchChange' && optionKeys[0] !== 'singleChange') {
      options[optionKeys[0]] = bindBatchChange_ONE(enableFunc)
    }
  }
  const methods = {}
  optionKeys.forEach(key => {
    methods[key] = options[key]
  })
  return { methods }
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 14:16:03
 * @description: 批量启禁用接口,且是分开的两个接口
 */
const bindBatchChange = (_enabledFunc, _disabledFunc) => {
  return function(row, enabled, { txt, tipTxt, type, paramKey = 'idList',paramKey2 = null ,sort = 'desc', sortCondition = 'id' }) {
    let that = this
    txt = txt ? txt : enabled ? '启用' : '禁用'
    tipTxt = tipTxt || `确认要${txt}所选内容?`
    type = type || 'warning'
    if (!row) {
      that.$confirm(tipTxt, '提示', { type }).then(() => {
        let idList = that.selectList.map(s => s.id)
        if (enabled) {
          _enabledFunc({ [paramKey]: idList ,enabled:true }, res => {
            that.$message.success(`${txt}成功`)
            that.search(that.page, { sort, sortCondition })
          })
        } else {
          _disabledFunc({ [paramKey]: idList,enabled:false  }, res => {
            that.$message.success(`${txt}成功`)
            that.search(that.page, { sort, sortCondition })
          })
        }
      })
    } else {
      let idList = [row.id]
      if (enabled) {
        _enabledFunc({ [paramKey]: idList,enabled:true }, res => {
          that.$message.success(`${txt}成功`)
          if (that.$route.params.id) {
            that.get()
          } else {
            that.search(that.page, { sort, sortCondition })
          }
        })
      } else {
        _disabledFunc({ [paramKey]: idList,enabled:true }, res => {
          that.$message.success(`${txt}成功`)
          if (that.$route.params.id) {
            that.get()
          } else {
            that.search(that.page, { sort, sortCondition })
          }
        })
      }
    }

  }
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 14:16:03
 * @description: 批量启禁用接口,且只有一个接口
 */
const bindBatchChange_ONE = _Func => {
  return function(
    row,
    enabled,
    { txt, tipTxt, type, paramKey = 'id', booleanKey = 'enabled', sort = 'desc', sortCondition = 'id' }
  ) {
    console.log(111)
    let that = this
    txt = txt ? txt : enabled ? '启用' : '禁用'
    tipTxt = tipTxt || `确认要${txt}所选内容?`
    type = type || 'warning'
    that.$confirm(tipTxt, '提示', { type }).then(() => {
      console.log(1111111111111111111111)
      let idList = row && row.id ? [row.id] : that.selectList.map(s => s.id)
      _Func({ [paramKey]: idList, enabled: enabled }, res => {
        that.$message.success(`${txt}成功`)
        if (that.$route.params.id) {
          that.get()
        } else {
          that.search(that.page, { sort, sortCondition })
        }
      })
    })
  }
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 14:16:10
 * @description: 单独启禁用 且是分开两个接口
 */
const bindSingleChange = (_enabledFunc, _disabledFunc) => {
  return function(row, enabled, { txt, paramKey = 'id',paramKey2 = null, sort = 'desc', sortCondition = 'id' }) {
    let that = this
    txt = txt ? txt : enabled ? '启用' : '禁用'
    let func = enabled ? _enabledFunc : _disabledFunc
    var params = {
      [paramKey]: row.id
    }
    if(paramKey2){
      params['enabled']=enabled
    }
    console.log(222222222222222222)
    func(params, res => {
      if (res == undefined) {
        // 控制开关启用失败则切换原来状态
        row.enabled = !row.enabled
        return false
      }
      that.$message.success(`${txt}成功`)
      if (this.$route.params.id) {
        that.get()
      } else {
        that.search(that.page, { sort, sortCondition })
      }
    })
  }
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 14:16:10
 * @description: 单独启禁用 且是一个接口
 */
const bindSingleChange_ONE = _Func => {
  return function(row, enabled, { txt, paramKey = 'id', sort = 'desc', sortCondition = 'id' }) {
    let that = this
    txt = txt ? txt : enabled ? '启用' : '禁用'
    console.log(33333333333333333333)
    _Func({ [paramKey]: row.id, enable: enabled }, res => {
      that.$message.success(`${txt}成功`)
      if (this.$route.params.id) {
        that.get()
      } else {
        that.search(that.page, { sort, sortCondition })
      }
    })
  }
}

/**
 * @author: cwlojako
 * @Date: 2020-09-10 14:16:10
 * @description: 没有批量启禁用  循环启禁用
 */
const bindSingleChangeCircle = (_enabledFunc, _disabledFunc) => {
  return function(row, enabled, { txt, tipTxt, type, paramKey = 'id', sort = 'desc', sortCondition = 'id' }) {
    let that = this
    console.log(666666)
    txt = txt ? txt : enabled ? '启用' : '禁用'
    tipTxt = tipTxt || `确认要${txt}所选内容?`
    type = type || 'warning'
    console.log(888,txt)
    that.$confirm(tipTxt, '提示', { type }).then(() => {
      let idList = that.selectList.map(s => s.id)
      console.log(idList)
      let func = enabled ? _enabledFunc : _disabledFunc
      let promises = idList.map(
        id =>
          new Promise(resolve => {
            func({ [paramKey]: id }, res => {
              resolve(res)
            })
          })
      )
      // 全部成功后再显示弹框
      Promise.all(promises).then(res => {
        that.$message.success(`${txt}成功`)
        that.search(that.page, { sort, sortCondition })
      })
    })
  }
}
