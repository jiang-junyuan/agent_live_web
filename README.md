# 直播后台

***
[一、项目介绍](#index1)

[二、项目展示](#index2)

[三、项目使用说明](#index3)

[四、其他](#index4)

## <span id="index1">项目介绍</span>

该项目为纺织市场实现开通线上模式所开发项目中的其中一个端 —— 主播后台。
主要负责帮助主播进行商品的推销、商品及订单数据的管理等。主要包含的模块有：直播间管理、直播回放管理、主播管理、短视频管理、商品管理、订单管理、用户信息管理、操作日志记录等。

## <span id="index2">项目展示</span>

登录

![请添加图片描述](https://img-blog.csdnimg.cn/54528347950a461a92c0e96b831b9057.jpeg)

直播间管理

![请添加图片描述](https://img-blog.csdnimg.cn/27912ad4a82f4d4498447c04dbd17219.jpeg)

直播间控制台

![请添加图片描述](https://img-blog.csdnimg.cn/92d2f6f533194979bd9af813255f6d41.jpeg)

直播间数据统计

![请添加图片描述](https://img-blog.csdnimg.cn/6ca7f4fe3ded4879923df5857489c4cf.jpeg)

直播回放详情

![请添加图片描述](https://img-blog.csdnimg.cn/3e3aa0401c164ee4a6cd32fcbe4ba794.jpeg)

主播管理

![请添加图片描述](https://img-blog.csdnimg.cn/e83552ec10644bd98b4f3720cdc5ec21.jpeg)

短视频草稿箱管理

![请添加图片描述](https://img-blog.csdnimg.cn/4bcc1cce42874d6bbee12bd8a57de0fd.jpeg)

短视频对应商品橱窗

![请添加图片描述](https://img-blog.csdnimg.cn/3c5219ba84a040bdac4859471c1b6d0a.jpeg)

已发布视频管理

![请添加图片描述](https://img-blog.csdnimg.cn/2205f10b75e346588a8c4441329f2249.jpeg)

短视频数据详情

![请添加图片描述](https://img-blog.csdnimg.cn/fed20c96c463427087e8ce17e4c7cf42.jpeg)

商品管理

![请添加图片描述](https://img-blog.csdnimg.cn/976070107ae14c9f8573b69038631f66.jpeg)

订单管理

![请添加图片描述](https://img-blog.csdnimg.cn/5f7dec3651a248e6bf17f43167fb04b1.jpeg)


个人中心    

![请添加图片描述](https://img-blog.csdnimg.cn/874d1cf656b148f1a10443fdf9c90584.jpeg)

![请添加图片描述](https://img-blog.csdnimg.cn/3861e928687b421d8a15ff16681f3b7d.jpeg)

登录日志

![请添加图片描述](https://img-blog.csdnimg.cn/08c3d9192fb04f3d949b51def2a23e94.jpeg)

操作日志

![请添加图片描述](https://img-blog.csdnimg.cn/2a5f99169a1b4c8b8d8347820d016c93.jpeg)

***

## <span id="index3">项目使用说明</span>

### 项目运行：

先将项目下载到自己电脑,然后分别执行：

`npm install`

`npm run dev`

### 项目打包：

`npm run build`

***

<span id="index4"></span>

觉得项目有帮助的小伙伴可以star项目哦，以防丢失。

同时也可以关注下作者的csdn博客，里面记录了作者在开发中所遇到的问题及解决方法，希望对你有帮助。

作者csdn博客链接：

<https://blog.csdn.net/jiangjunyuan168?type=blog>